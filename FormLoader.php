<?php


class FormLoader
{
    private $action;
    private $httpMethod;
    
    public function __construct($action,$httpMethod)
    {
        $this->action = $action;
        $this->httpMethod = $httpMethod;
        
    }

   

    public function createForm($selectOption)
    {
        $oblasti = new OblastiTest();
        $validator = new Validator();
        $formCreator = 
        "<div class ='container'>"
        ."<div class ='justify-content-md-center'>"         
        ."<form action='$this->action' method='$this->httpMethod'>"
        ."<input class='form-control' type='text' name='selishteName' placeholder='с. Беляковец'>"
        ."<br>"
        ."<select id='oblasti' name='oblasti' class='form-control'>"."<option value='0'>----------</option>".$selectOption."</option>"."</select>" 
        ."<br>" 
        ."<input type='submit'  class='btn btn-primary'>"
        ."</form>"
        ."</div>"
        ."</div>"
        ;

        return $formCreator;
    }
}