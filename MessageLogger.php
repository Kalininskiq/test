<?php
require_once __DIR__.'/vendor/autoload.php'; 
class MessageLogger
{
    public static function  logMessage($message)
    {
        $message .= "<p>". $message ."</p>";
        return $message;
    }

    public static function logSuccessMessage($message)
    {
       return  $message ="<div class='alert alert-success' role='alert'>".$message."</div>";
        
    }

    public static function logErrorMessage($message)
    {
        return $message = "<div class='alert alert-danger' role='alert'>".$message."</div>";
       
    }

    public  static function logAlertMessage($message)
    {
        return $message =  "<div class='alert alert-warning' role='alert'>".$message."</div>";
       
    }
}