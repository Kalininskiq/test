
<?php 
require_once __DIR__.'/vendor/autoload.php'; 

use Ekatte\Oblast;
use Ekatte\Obshtina;
use Ekatte\Selishte;





//Will remove constant message variable assignment and returning of that variable
// var_dump($_POST);
// die();

class  OblastiTest
{
    private $oblastName = "VTR";
    

    public function getObshtiniNames()
    {
        $names = array();



        $obshtini = new Obshtina();
        $obshtini = $obshtini->getObshtiniList("VTR");
        foreach ($obshtini as $obshtina) {
            $names[$obshtina[0]] = $obshtina[2];
        }

        return $names;
    }


    public function setSelectTagOption()
    {
        $values = array_values($this->getObshtiniNames());
        $options = "";
        foreach ($values as $name) {
            $options .= '<option value="' . $name . '">' . $name . '</option>';
        }

        return $options;
    }

    public function getSelishtaNames($httpMethodOption)
    {
        $isSelectedObshtina  = (empty($_POST[$httpMethodOption])) ? "Задайте стойност на община" : $_POST[$httpMethodOption];
        $selectedObshtina = $isSelectedObshtina;
        $newObshtinaKey = null;
        $obshtini = $this->getObshtiniNames();
        $names = array();
        $message = "";

        $selishta = new Selishte();


        foreach ($obshtini as $key => $value) {
            if ($selectedObshtina == $value) {

                $newObshtinaKey = $key;
            }
        }
        

        return $names = $selishta->getSelishtaList('VTR', $newObshtinaKey);
    }

    public function CheckSelishte($httpMethodOption)
    {
        $isTypesSelishte  = (empty($_POST[$httpMethodOption])) ? "" : $_POST[$httpMethodOption];
        $typedSelishte = $isTypesSelishte;
        $selishta = $this->getSelishtaNames("oblasti");
        $names = array("");
        $message = " ";
        foreach ($selishta as $name) {
            $names[] = array_push($names, $name[0]);
        }
        if (empty($typedSelishte) || empty($_POST["oblasti"]) ) {

           echo "";

        } else if (in_array($typedSelishte, $names)) {
            echo MessageLogger::logSuccessMessage("Съществува"); 

        } else {
            echo MessageLogger::logErrorMessage("Не съществува");
        }

 
    }

    public function  getLastSearch($httpMethodOption)
    {
        $isTypedSelishte  = (empty($_POST[$httpMethodOption])) ? "" : $_POST[$httpMethodOption];
        $typedSelishte = $isTypedSelishte;
      
        if (empty($typedSelishte)) {
            echo " Потърсихте селище -------";
        } else {
            echo " Потърсихте селище: ".$typedSelishte;
        }

    }



}

$formdata = $_POST;
$oblastiObject = new OblastiTest();
$validator = new Validator();


$formLoader = new FormLoader("index.php","post");
echo $formLoader->createForm($oblastiObject->setSelectTagOption());

  $validator->validateForm($formdata);
  $oblastiObject->CheckSelishte("selishteName");
  $oblastiObject->getLastSearch("selishteName");

 ?>



<head> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>